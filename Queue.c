# include "Queue.h"
# include <stdlib.h>
# include <stdio.h>

void printarr(item *arr, uint32_t h, uint32_t t)
{
    //printf("SWAP WITH: \n");
    printf("\nh:     %*s%d\n    {", h*5, "H", h);
    for(int k = 0; k < 256; k++)
    {
        if(arr[k].weight)
        {
            printf("%3lu, ", arr[k]);
        }
        else
        { printf("0x0, ");}
    }
    printf("}\n");
    printf("t:     %*s%d\n", t*5, "T", t);

}

queue *newQueue(uint32_t size)
{
    queue *q = malloc(sizeof(queue));
    q -> size = size + 1; //allocate for one larger.
    q -> head = 0;
    q -> tail = 0;
    q -> Q = calloc(q->size, sizeof(item));
    return q;
}
void delQueue(queue *q)
{
    free(q -> Q);
    free(q);
    q = NULL;
}

bool emptyQueue(queue *q)
{
    return q -> head == q -> tail;
}
bool fullQueue(queue *q)
{
    return (((q -> head) + 1) % q -> size ) == q -> tail;
}

bool enqueue(queue *q, item i)
{
    //printf("\nEnqueuing %u:\n", i);
    //printarr(q->Q, q->head, q->tail);
    if(!fullQueue(q))
    {
        uint32_t h = q->head++;
        for(; (h %q->size) != q->tail; h = PREV(h, q->size))
        {
            //printarr(q->Q, h, q->tail);
            if(q->head >= q ->size)
            {
                q -> head = 0;
            }


            if(q->Q[PREV(h, q->size)].weight <= i.weight)
            {
                q->Q[h] = i; //insert in correct position.
                //printarr(q->Q, h, q->tail);
                return true;
            }else
            {
                SWAP(q->Q[h], q->Q[PREV(h, q->size)]) //percolate down.
                //printarr(q->Q, h, q->tail);
            }
        }
        if(h == q->tail || q->Q[PREV(h, q->size)].weight <= i.weight )
        {
            q->Q[h] = i; //insert in correct position
            //printarr(q->Q, h, q->tail);
            return true;
        }
    }
    return false;
}

bool dequeue(queue *q, item *i)
{
    //printf("\nDequeing!\n");
    if(!emptyQueue(q))
    {
        *i = q ->Q[q->tail++];
        if(q->tail >= q ->size)
        {
            q -> tail = 0;
        }
        return true;
    }
    return false;
}
