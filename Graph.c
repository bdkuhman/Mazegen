#include "Graph.h"
#include "List.h"
#include "Queue.h"
#include <stdlib.h>
#include <string.h> //memset;
#include <stdio.h>

#define MAXEDGE 4

#define printLength 28

Graph* newGraph(int order){ //graph data structure;
    Graph* g = malloc(sizeof(Graph)); //allocate container.
    g -> size = 0; //edges
    g -> order = order; //vertices
    g->adj = memset(malloc(sizeof(ListNode*) * order), 0x0, order * sizeof(ListNode*));
    g->color = memset(malloc(sizeof(int) * order), WHITE, order * sizeof(int)); //set node color to white;
    g->dist = memset(malloc(sizeof(int) * order), INF, order * sizeof(int)); //set node distances to infinite;
    g->parent = memset(malloc(sizeof(int) * order), -1, order * sizeof(int)); //set parents to null.
}

void delGraph(Graph* G){ //free graph.
    for (int i = 0; i < G->order; i++) {
        if(G->adj[i] != NULL){
            delList(G->adj[i]);
        }
    }
    free(G->adj);
    free(G->color);
    free(G->dist);
    free(G->parent);
    free(G);

}

void addEdge(Graph* G, int u, int v){ //create an edge.
    int i = rand() % MAXEDGE; //random edge weights.
    append(&(G->adj[u]), newNode(v, i));
    append(&(G->adj[v]), newNode(u, i));
    G->size++;
}

void addDirEdge(Graph* G, int u, int v, int w)
{
    append(&(G->adj[u]), newNode(v, w));
    G->size++;
}

char* itoa(int val, int base){
    if(val == 0)
    {
        return "0";
    }
	static char buf[32] = {0};
	int i = 30;
	for(; val && i ; --i, val /= base)
		buf[i] = "0123456789abcdef"[val % base];
	return &buf[i+1];
}

char* adjString(ListNode* n)
{
    char* out = calloc(1, printLength);
    for(n; n != NULL; n= n->next){
        strcat(out, "[");
        strcat(out, itoa(n->id, 10));
        strcat(out, ",");
        strcat(out, itoa(n->weight, 10));
        strcat(out, "] ");
    }
    return out;
}

void print(Graph* G){
    printf("%2s | %-28s | %2s | %2s | %2s |\n", "N", "adj", "C", "D", "P");
    printf("----------------------------------------------\n");
    for(int i = 0; i < G->order; i++)
    {

        printf("%2d | %-28s | %2d | %2d | %2d |\n", i, adjString(G->adj[i]), G->color[i], G->dist[i], G->parent[i]);
        fflush(stdout);
    }
    printf("\n");

}

void prim(Graph* G, Graph* tree){
    queue* q = newQueue(G->size);
    int start = rand() % G->order; //pick random start;
    G->color[start] = BLACK;

    for(ListNode* s = G->adj[start]; s != NULL; s = s->next){
        enqueue(q, (item){start, s->id, s->weight});
        G->color[s->id] = GRAY;
    }

    while(!emptyQueue(q)){
        item i;
        dequeue(q, &i);
        bool flag = false;
        for(ListNode* n = tree->adj[i.src]; n != NULL; n = n->next)
        {
            if(n->id == i.dest){
                flag = true;
            }
        }
        for(ListNode* n = tree->adj[i.dest]; n != NULL; n = n->next)
        {
            if(n->id == i.src){
                flag = true;
            }
        }

        if( !flag && tree->parent[i.dest] == -1)
        {
            addDirEdge(tree, i.src, i.dest, i.weight);
            G->color[i.src] = BLACK;
            tree->parent[i.dest] = i.src;
            tree->dist[i.dest] = tree->dist[i.src] + 1;

            for(ListNode* s = G->adj[i.dest]; s != NULL; s = s->next){
                if(s->id != i.src){
                    enqueue(q, (item){i.dest, s->id, s->weight});
                }
                G->color[s->id] = GRAY;
            }
        }
    }
    delQueue(q);
    //print(tree);
    //print(G);
}
