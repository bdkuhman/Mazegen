#include "heap.h"
#include <stdio.h>
#include <stdlib.h>

//initializes the heap pointer to prepare it for construction
HEAP* Initialize(int n ){

	//initializing heap
	HEAP* heap = malloc(sizeof(HEAP));

	//capacity given by user
	heap->capacity = n;

	//initial size set to 0 since it is empty
	heap->size = 0;

	//H array set to NULL to make sure there aren't any issues when
	//filling later on
	heap->H = NULL;

	//returning initialized heap pointer
	return heap;

}//end Initialize

//method for recurring down the tree to make sure the minimum is on top
HEAP* MIN_HEAPIFY(HEAP* heap, int root, int n, int size){

	//smallest initially assumed to be root
	int smallest = root;

	//left child is always 2*root
	int left = 2*root;

	//right child always 2*root+1
	int right = 2*root + 1;

	//checking value at left child compared to current smallest
	if( left <= size && heap->H[left]->key <= heap->H[smallest]->key){

		smallest = left;

	}//end if

	//checking value of right child compared to current child
	if( right <= size && heap->H[right]->key <= heap->H[smallest]->key){

		smallest = right;

	}//end if

	//recurring down the tree if it was not already a min heap
	if( smallest != root){

		//standard swapping algorithm
		ELEMENT* temp = heap->H[root];

		heap->H[root] = heap->H[smallest];

		heap->H[smallest] = temp;

		heap = MIN_HEAPIFY(heap, smallest, n, size);

	}//end if

	//returns the heapified heap pointer
	return heap;

}//end MIN_HEAPIFY

//builds the heap out of the array A filled with ELEMENTS and
//ensures it follows the min heap property
HEAP* BuildHeap( HEAP* heap, ELEMENT** A, int n, int size){

	//assigning H field
	heap->H = A;

	//run heapify on the parent of the last leaf and then recur
	//on each node before it
	for(int minParent = size/2; minParent > 0; minParent--){

		heap = MIN_HEAPIFY(heap, minParent, n, size);

	}

	//returning finished heap
	return heap;

}//end BuildHeap

//inserts a new element into the heap and prints if flag is 1
HEAP* Insert( HEAP* heap, int keyK, int flag, int* size, int* n){

	//checking if there is room for the element
	if( *size+1 >= *n ){

		//doubling capacity if there isn't room
		int capacity = *n;

		capacity = capacity * 2;

		*n = capacity;

		heap->H = (ELEMENT**) realloc( heap->H, (*n) * sizeof(ELEMENT*) );

	}//end if

	//checking if it should print
	if( flag == 1 ){

		printHeap( heap, *size, *n );

	}//end if

	//setting new size
	*size = *size + 1;

	heap->H = (ELEMENT**) realloc( heap->H, (*size) * sizeof(ELEMENT*) );

	heap->H[*size]->key = keyK;

	//ensuring it follows min heap properties
	heap = BuildHeap( heap, heap->H, *n, *size );

	if( flag == 1 ){

		printHeap( heap, *size, *n );

	}//end if

	return heap;

}//end Insert

//prints out the heap
void printHeap( HEAP* heap, int size, int n ){

	printf("The capacity is %d\n", n)

	printf("The size is %d\n", size)

	for( int i = 1; i <=size; i++){

		printf("%d\n", heap->H[i]->key);

	}//end for

}//end printHeap

//removes the root and re-builds the heap, then returns the smalles element
ELEMENT* DeleteMin( HEAP* heap, int* size, int n ){

	int root = 1;

	ELEMENT* smallest = heap->H[root];

	heap->H[1] = heap->H[*size];

	*size = *size - 1;

	heap = BuildHeap( heap, heap->H, n, *size );

	return smallest;

}//end DeleteMin

//removes the minimum element in the heap and prints the heap if flag is 1
void minRemover( HEAP* heap, int* size, int n, int f){

	if( f == 1 ){

		printHeap( heap, *size, n );

	}//end if

	ELEMENT* smallest = malloc(sizeof(ELEMENT));

	//getting smalles from DeleteMin
	smallest = DeleteMin( heap, size, n );

	if( f == 1 ){

		printHeap( heap, *size, n );

	}//end if

		//printing the smallest element
		printf("%d\n",smallest->key);

		free(smallest);

}//end minRemover

//decreases the key of an element in the heap and prints if flag is 1
HEAP* DecreaseKey( HEAP* heap, int index, int newKey, int f, int size, int n ){

	//checks to make sure the element is in the list
	if( index > size ){

		cout << "There are only " << size << " elements in the heap. Hence this operation can not be completed."<< endl;

	}

	else{

		if( f == 1 ){

			printHeap( heap, size, n );

		}//end if

		//decreasing key
		heap->H[index]->key = newKey;

		//rebuilding heap to keep the min heap property
		heap = BuildHeap( heap, heap->H, n, size );

		if( f == 1 ){

			printHeap( heap, size, n );

		}//end if

	}//end else

	return heap;

}//end DecreaseKey
