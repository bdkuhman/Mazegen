#include "List.h"
#include <stdlib.h>

ListNode *newNode(int id, int weight){
    ListNode* node = malloc(sizeof(ListNode));
    node -> id = id;
    node -> weight = weight;
    node -> next = (ListNode *) NULL;
    node -> prev = (ListNode *) NULL;
    return node;
}

void append(ListNode** head, ListNode* node){
    if(*head == NULL)
    {
        *head = node;
    }
    else
    {
        ListNode* index = *head;
        while(index->next != NULL)
        {
            index = index -> next;
        }
        node -> prev = index;
        index -> next = node;
    }
}

void prepend(ListNode** head, ListNode* node){
    node -> next = *head;
    (*head) -> prev = node;
    node -> prev = 0x0;
    *head = node;
}

void delNode(ListNode* node){
    free(node);
    node = NULL;
}

void delList(ListNode* head){
    if(head -> next != NULL)
    {
        delList(head -> next);
    }
    head -> id = 0;
    head -> weight = 0;
    head -> next = NULL;
    head -> prev = NULL;
    free(head);
    head = NULL;
}
