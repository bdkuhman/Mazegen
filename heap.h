//ELEMENT and HEAP must be declared in heap.h so as to avoid forward declaration
//and incomplete data type errors

//definition of ELEMENT data type
struct ELEMENT{

	int key;

};//end element

//definition of HEAP data type
struct HEAP{

	int capacity;

	int size;

	ELEMENT** H;

};//end heap

//creates an empty heap
HEAP* Initialize( int );

//heapify method to make sure smallest is at the root
HEAP* MIN_HEAPIFY(HEAP* heap, int root, int n, int size);

//builds the heap given an array filled with elements
HEAP* BuildHeap( HEAP* , ELEMENT**, int, int );

//Inserts an element with key k and specified flag
HEAP* Insert( HEAP*, int, int, int*, int* );

//deletes the element with the minimum key and returns it
ELEMENT* DeleteMin( HEAP*, int*, int);

//decreases the key of a given index
HEAP* DecreaseKey( HEAP*, int, int, int, int, int );

//prints out the heap information
void printHeap( HEAP*, int, int );

//retrievs the smallest element and deletes it, printing if told to do so
void minRemover( HEAP*, int*, int, int );
