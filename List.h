#ifndef MAZE_LIST_H
#define MAZE_LIST_H
#ifndef INF
#define INF -1
#endif

typedef struct ListNode{
    int id;
    int weight;
    struct ListNode* next;
    struct ListNode* prev;
}ListNode;

ListNode *newNode();

void append(ListNode** head, ListNode* node);

void prepend(ListNode** head, ListNode* node);

void delNode(ListNode* node);

void delList(ListNode* head);

#endif //MAZE_LIST_H
