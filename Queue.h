# define PREV(x, n) (x + n - 1) % n
# define SWAP(x, y) {item t = x; x = y; y = t;}

# ifndef _QUEUE_H
# define _QUEUE_H
# include <stdint.h>
# include <stdbool.h>

# ifndef _ITEM_T
# define _ITEM_T

typedef struct item {
    int src;
    int dest;
    int weight;
} item;

#endif

typedef struct queue
{
    uint32_t size;
    uint32_t head, tail;
    item *Q;
} queue;

queue *newQueue(uint32_t);
void delQueue(queue *);

bool emptyQueue(queue *);
bool fullQueue(queue *);

bool enqueue(queue *, item);
bool dequeue(queue *, item *);

void printarr(item *arr, uint32_t h, uint32_t t);
# endif
