STD= -std=c99
FLAGS = $(STD)
CC = gcc $(FLAGS)
EFLAGS = -pedantic -Wall -Wextra -Werror
OBJECTS = main.o Bitmap.o Graph.o List.o Queue.o
BINARIES = mazegen mazegen.exe

.PHONY:
all: mazegen

extra: FLAGS = $(STD) $(EFLAGS)
extra: clean mazegen

mazegen : $(OBJECTS)
	$(CC) $(OBJECTS) -o mazegen -lm

Queue.o : Queue.c Queue.h
	$(CC) -c -g Queue.c

Graph.o : Graph.c Graph.h List.h
	$(CC) -c -g Graph.c

List.o : List.c List.h
	$(CC) -c -g List.c

Bitmap.o : Bitmap.c Bitmap.h
	$(CC) -c -g Bitmap.c

main.o : main.c Graph.h Queue.h Bitmap.h
	$(CC) -c -g main.c

.PHONY:
clean :
	rm -f $(OBJECTS) $(BINARIES) $(wildcard *.bmp)
