#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <getopt.h>
#include <string.h>

#include "Bitmap.h"
#include "Graph.h"
#include "Queue.h"

//converts a grid coordinate to a grid node id.
#define toID(x, y, h) ((y*h)+x)
#define IDtoX(n, w) ((2*(n%w))+1)
//#define IDtoY(n, w, h) (h - 2 - 2 * n / (w - 1) * 2)
#define IDtoY(n, h, w) (-1 * ((2*(n/w))+1) + (2*h))
#define MID(a, b) ((a+b)/2)

#define MAX_NAME 25

void test(int w, int h)
{
  int Gw = w/2;
  int Gh = h/2;
  int Gsize = Gw*Gh;
  int x, y;
  for(int n = 0; n < Gsize; n++){
    x = IDtoX(n, Gw);
    y = IDtoY(n, Gw, Gh);
    printf("N: %d, X: %d, Y: %d\n", n, x, y);
  }

}

//remove walls between connected nodes.
void removeWalls(bitmap* map, Graph* G)
{
    for(int i = 0; i < G->order; i++) //for all nodes
    {
        ListNode* node = G->adj[i]; //get all connected nodes.

        if(node != NULL){
            for(node; node != NULL; node = node -> next)
            {
                int x1 = IDtoX(node->id, (map->infoHeader.width/2));
                int y1 = IDtoY(node->id, (map->infoHeader.height/2), (map->infoHeader.width/2));
                int x2 = IDtoX(i, (map->infoHeader.width/2));
                int y2 = IDtoY(i, (map->infoHeader.height/2), (map->infoHeader.width/2));
                int Xc = MID(x1, x2);
                int Yc = MID(y1, y2);
                setPixel(map, Yc, Xc, WHITE_COLOR);
            }
        }
    }
}

//init the maze.
void initMaze(bitmap* map, Graph* graph, uint32_t imageWidth, uint32_t imageHeight)
{
    int gridWidth = imageWidth/2; int gridHeight = imageHeight/2;

    //cycle through pixels
    for ( int i = 0; i < imageWidth; i++ )
    {
        for ( int j = 0; j < imageHeight; j++ )
        {
            //set the alternating ones to white.
            if ( i * j % 2 )
            {
                int gridx = i/2; int gridy = j/2; //convert pixel to graph coordinates.
                int gridID = (gridy * gridHeight) + gridx; //create an incrementing ID based on position.
                setPixel(map, j, i, WHITE_COLOR); //make it white

                if(gridy != gridHeight-1)// if not top row
                {
                    addEdge(graph, toID(gridx, gridy, gridWidth), toID(gridx, (gridy+1), gridWidth));
                }
                if(gridx != gridWidth-1) //if not right edge.
                {
                    addEdge(graph, toID(gridx, gridy, gridWidth), toID((gridx+1), gridy, gridWidth));
                }
                int id = toID(gridx, gridy, gridWidth);
            }
            else setPixel(map,j, i, BLACK_COLOR); //Add walls between nodes
        }
    }
}

int main(int argc, char** argv) {
    char c;
    char* filename = calloc(1, MAX_NAME);
    strcpy(filename, "out.bmp");

    int imageWidth = 11;
    int imageHeight = 11;
    srand(time(NULL));
    while((c = getopt(argc, argv, "w:h:o:s:d")) != -1)
    {
        switch(c){
            case 'w':
                imageWidth = atoi(optarg);
                imageWidth = imageWidth % 2 == 0 ? imageWidth + 1 : imageWidth;
                imageWidth = abs(imageWidth);
                break;
            case 'h':
                imageHeight = atoi(optarg);
                imageHeight = imageHeight % 2 == 0 ? imageHeight + 1 : imageHeight;
                imageHeight = abs(imageHeight);
                break;
            case 'o':
                filename = strcpy(filename, optarg);
                break;
            case 'd':
              test(imageWidth, imageHeight);
              return 0;
              break;
            case 's':
              srand(atoi(optarg));
              break;
            case '?':
                printf("Invalid Argument: %c\n", c);
            default:
                abort();
        }
    }

    item* i;
    int gridWidth = imageWidth/2;
    int gridHeight = imageHeight/2;

    bitmap* map = newMap(imageWidth, imageHeight);

    Graph* graph = newGraph(gridWidth * gridHeight);
    Graph* tree = newGraph(graph->order);

    initMaze(map, graph, imageWidth, imageHeight);

    prim(graph, tree);
    removeWalls(map, tree);

    toFile(filename, map);
    freeMap(map);
    delGraph(graph);
    delGraph(tree);
    free(filename);
}
