#!/bin/bash

for w in `seq 25 2 50`; do
	for h in `seq 35 2 70`; do
		$(./mazegen -w $w -h $h)
		real=$(wc -c < out.bmp)
		reported=$((16#$(xxd -a -l4 -s 2 -e out.bmp | awk '{print $2}')))
		rm -f out.bmp
		if [ $real != $reported ]
		then
			echo "$w,$h,$real,$reported"
		fi
	done
done
